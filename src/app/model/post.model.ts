import { User } from './user.model'

export class Post {
    id: number
    body: string
    title: string
    userId: number
    user: User
    total_number_of_comments: number
}