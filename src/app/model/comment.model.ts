import { Post } from './../model/post.model';

export class Comment {
    id: number
    name: string
    email: string
    body: string
    postId: number
    post: Post
}