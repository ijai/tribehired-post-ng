import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Post } from 'src/app/model/post.model';
import { CommentService } from 'src/app/services/comment.service';
import { PostService } from 'src/app/services/post.service';
import { Comment } from './../../model/comment.model';

@Component({
    selector: "app-post-dtl",
    templateUrl: "./post-details.component.html",
    styleUrls: ["./post-details.component.scss"],
})
export class PostComponent implements OnInit {

    id: number;
    post: Post = new Post()
    commentArr: Comment[] = new Array<Comment>()
    commentsViewed = false

    constructor(
        private actR: ActivatedRoute,
        private _post: PostService,
        private _comment: CommentService
    ) {
        this.id = +this.actR.snapshot.paramMap.get("id")
    }
    
    ngOnInit() {
        this._post.getSinglePost(this.id).subscribe(
            data=>{
                this.post = data
            }
        )
    }

    search(e){
        let searchTerm = e.target.value
        this._comment.getCommentOnPost(this.id, searchTerm).subscribe(
            data=>{
                this.commentArr = data
            }
        )
    }

    viewComments(){
        this.commentsViewed = true
        this._comment.getCommentOnPost(this.id).subscribe(
            data=>{
                this.commentArr = data
            }
        )
    }
}
