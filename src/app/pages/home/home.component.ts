import { Component, OnInit } from "@angular/core";
import { Post } from 'src/app/model/post.model';
import { PostService } from 'src/app/services/post.service';

@Component({
    selector: "app-home",
    templateUrl: "./home.component.html",
    styleUrls: ["./home.component.scss"],
})
export class HomeComponent implements OnInit {

    postArr : Post [] = new Array<Post>();

    constructor(
        private _post: PostService
    ) {}
    
    ngOnInit() {
        this._post.getAllPost().subscribe(
            data=>{
                this.postArr = data
            }
        )
    }
}
