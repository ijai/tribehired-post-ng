import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Post } from '../model/post.model';
import { environment } from './../../environments/environment';

const POST_URL = `${environment.API_URL}api/posts`;

@Injectable({
    providedIn: 'root'
})
export class PostService {

    constructor( 
        private httpCLient: HttpClient
    ){}

    getAllPost(): Observable<Post[]>{
        return this.httpCLient.get<Post[]>(POST_URL)
    }

    getSinglePost(id: number): Observable<Post>{
        return this.httpCLient.get<Post>(POST_URL+"/"+id)
    }
}