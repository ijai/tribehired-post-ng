import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Comment } from './../model/comment.model';

import { environment } from './../../environments/environment';

const COMMENT_URL = `${environment.API_URL}api/comments`;

@Injectable({
    providedIn: 'root'
})
export class CommentService {

    constructor(
        private httpClient: HttpClient
    ){ }

    getCommentOnPost(id: number, searchTerm ?: string): Observable<Comment[]>{
        let url = COMMENT_URL+"?postId="+id;
        if(searchTerm){ 
            url += "&searchTerm="+searchTerm;
        }
        return this.httpClient.get<Comment[]>(url)
    }
}